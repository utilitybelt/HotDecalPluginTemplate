﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HotDecalPluginTemplateInstaller {
    public partial class DecalHotReloadPluginUBServiceTemplateWizardUI : Form {
        private static string customMessage;
        private TextBox textBox1;
        private Button button1;
        private CheckBox checkbox1;

        public DecalHotReloadPluginUBServiceTemplateWizardUI() {
        }
        public static string CustomMessage {
            get {
                return customMessage;
            }
            set {
                customMessage = value;
            }
        }
        private void button1_Click(object sender, EventArgs e) {
            customMessage = textBox1.Text;
            this.Close();
        }

        private void InitializeComponent() {
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();

            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(72, 91);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(50, 25);
            this.button1.TabIndex = 0;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(10, 25);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(70, 20);
            this.textBox1.TabIndex = 1;
            // 
            // DecalHotReloadPluginUBServiceTemplateWizardUI
            // 
            this.ClientSize = new System.Drawing.Size(400, 150);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Name = "DecalHotReloadPluginUBServiceTemplateWizardUI";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Decal Plugin w/ Hot-Reload and UtilityBelt.Service";
            this.Load += new System.EventHandler(this.DecalHotReloadPluginUBServiceTemplateWizardUI_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private void DecalHotReloadPluginUBServiceTemplateWizardUI_Load(object sender, EventArgs e) {

        }
    }
}
