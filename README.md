﻿# 🚀  Hot Decal Plugin Template

A hot-reloadable [Decal](https://decaldev.com/) v2.9.8.3 plugin template for Visual Studio 2022 Community Edition that builds against .NET Framework 4.8 and [UtilityBelt.Service](https://gitlab.com/utilitybelt/utilitybelt.service) for the views.

## 🌟 Features
 
 * 👀 Uses UtilityBelt.Service's [ImGui](https://github.com/ocornut/imgui) implementation for the view system.
 * 📅 Builds against .NET Framework 4.8 (Windows 7+) with C\# language version 10.
 * 🔥 Hot-reload plugin code during development, without logging out / restarting the client.
 * 💾 Includes [NSIS](https://nsis.sourceforge.io/Main_Page) installer generation.
   * Downloads and installs .NET Framework 4.8 if not already installed.
   * Bundles UtilityBelt.Service installer.
 * 🌍 Builds on both windows / linux (maybe osx too?).
 * 🦊 Includes example `.gitlab-ci.yml` for building / deploying gitlab packages automatically on every commit.

## 💻 Template Installation

  * Install from the [Visual Studio Marketplace](https://marketplace.visualstudio.com/items?itemName=UtilityBelt.HotDecalPluginTemplates).
  * Alternatively, you can download and run the vsix file from the [Releases](https://gitlab.com/utilitybelt/HotDecalPluginTemplate/-/releases) page.

## 📝 Creating a New Plugin

  * Install [Decal v2.9.8.3](https://decaldev.com/) or greater
  * Install [Visual Studio 2022 Community Edition](https://visualstudio.microsoft.com/vs/community/)
    * Install `.NET Destktop Development` workload. Make sure the `.NET Framework 4.8 SDK` is installed.
  * Install the plugin template from one of the sources above.
  * Launch Visual Studio 2022, and choose `Create a new project`
  * Create a new project, and choose `Decal Plugin w/ Hot-Reload and UtilityBelt.Service`. ![Choose Visual Studio 2022 project template dialog for a new project.](https://i.imgur.com/kd3rfb6.png)
  * It is recommended to check the box `Place solution and project in the same directory`. ![Visual Studio 2022 configure new project dialogue](https://i.imgur.com/zK0jYp6.png)
  * Leave the selected framework as `.NET Framework 4.8`
  * Click `Create`.

## 🔧 Developing Your Plugin

  * The plugin code is located in the `<YourPlugin>` project. You can ignore the `<YourPlugin>.Loader` project, it contains the hot-reloader and doesn't need to be edited unless you are doing something out of the ordinary.
    * `<YourPlugin>/PluginCore.cs` is the main Plugin class. It contains the plugin startup / shutdown methods.
    * `<YourPlugin>/ExampleUI.cs` includes some demo UI code.
    * `installer.nsi` is the NSIS script used to generate the installer.
  * Build the new plugin solution in Visual Studio with `Ctrl+Shift+B`, or by going to `Build -> Build Solution`.
  * Add both `<YourPlugin>.dll` and `<YourPlugin>.Loader.dll` to decal by opening decal from the tray and selecting `Add -> Browse -> <YourProjectPath>/bin/Release/net481/`. (Select each dll file individually.)
  * Disable `<YourPlugin>` in decal by unchecking it under the `Plugins` list.
    * During development you should have `<YourPlugin>` (under `Plugins`) disabled, and `<YourPlugin>.Loader` enabled (under `Network Filters`) in decal. This allows for hot-reloading of the plugin without logging out / restarting the client.
    * To hot-reload, just recompile the plugin while ingame. You should see a message in the chat window showing that the plugin has reloaded.

## 📦 Releasing Your Plugin

  * Right click the Plugin project in Visual Studio and choose `Properties`. Scroll down and update the version number.
  * Build the latest release version.
  * In decal, enable your Plugin under the `Plugins` section, and disable `<YourPlugin>.Loader` under Network Filters. This allows you to test the plugin with hot-reloading disabled.
  * Ensure the plugin works as expected ingame.
  * Test the installer in `bin/Release/`.
  * Distribute the installer.
  
## 🗄️ Build Server Requirements
  * Either use the docker image at `` use a build server with the following requirements:
    * All build servers:
      * [Powershell](https://learn.microsoft.com/en-us/powershell/) is in the environment `PATH` by calling `powershell`.
    * Non-Windows build servers:
      * [NSIS](https://nsis.sourceforge.io/Main_Page) is in the environment `PATH` by calling `makensis`.
      * DotNet 6 SDK installed.
  
## 💡 Tips

  * If you need to reference more decal dlls, make sure to copy them to `deps/` and reference from there to maintain linux build compatibility.
  * When hot-reloading, events like `CharacterFilter.LoginComplete` have already triggered when the plugin reloads so the plugin will never see them. During plugin startup, you can check the current login state to determine if this is a normal load, or a hot one.
  ```csharp
    protected override void Startup() {
        var isHotReload = CoreManager.Current.CharacterFilter.LoginStatus == 3;
    }
  ```
  * If hot-reloading is being prevented because `<YourPlugin>.Loader` is trying to be recompiled and is locked by acclient, you can right click the `<YourPlugin>.Loader` project in the Visual Studio `Solution Explorer` and select `Unload Project` to prevent it from being rebuilt. **Note:** You must build `<YourPlugin>.Loader` at least once before unloading the project, if you want to use hot-reloading.

## 👷 Roadmap

  * A "Kitchen Sink" demo that showcases all the functionality of UtilityBelt.Service.
  * Decal Filter / Service templates.
  * Add automated testing
  * GitHub Actions example
  
## 👁‍🗨 Example Plugins

  * [LBVisualizer](https://gitlab.com/trevis/lbvisualizer)
  * [UtilityFace](https://github.com/aquafir/UtilityFace)

## Changelog

  * v1.1.0
    * Updated UtilityBelt.Service to 2.2.6
    * Fixed issue with PluginCore.AssemblyDirectory not being properly set on released builds
    * Updated decal refs to v2.9.8.3
    * Updated default language version to 12
    * Fixed issue with spaces in the build directory
    * Fixed issue with installer not using curly braces around plugin class id in the registry entry
 
## 📄 License

MIT
